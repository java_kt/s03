import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        // Loops are control structures that allow code blocks to be executed multiple times
        // Types of loops
        // 1. For loop
        // 2. While loop
        // 3. Do-while loop

        // FOR LOOPS
        // For loops have 3 expressions inside of it.
        // Initial condition
        // Limiting expression
        // Step increment or decrement

        for(int i = 0; i < 10; i++){
            //int i = 0 -> initial condition
            //i < 10 -> limiting expression
            //i++ -> increment/decrement
            System.out.println("Current count: " + i);
        }

        // For loops can be used to loop through the contents of an array
        int[] intArray = {100,200,300,400,500};
        for (int i = 0; i < intArray.length; i++) {
            System.out.println(intArray[i]);
        }

        // Another way to loop through arrays is called the foreach or the enhanced for loop.
        String[] nameArray = {"John", "Paul", "George", "Ringo"};
        for (String name : nameArray){
            System.out.println(name);
        }

        // NESTED FOR LOOPS
        // for loops can also be nested together. It can be useful when traversing multi-dimensional arrays and similar data structures.

        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        for (int row = 0; row < 3; row++){ //outer loop will loop through the rows
            for (int col = 0; col < 3; col++){ //inner loop will loop through the columns in each row.
                System.out.println(classroom[row][col]);
            }
        }

        // While and Do-while loops
        // While loops allow for repetitive use of code, similar to for-loops but are usually used for situations where the content to iterate through is indefinite.
        // Do while loops are similar to while loops. However, do-while loops will always execute at least once - while loops may not execute at all.

        int x = 0;
        int y = 10;

        while(x < 10){
            System.out.println("Loop number: " + x);
            x++;
        }

        do{
            System.out.println("Countdown: " + y);
            y--;
        } while (y > 10);

        // Inputs and Exception handling
        // If outputting via the console is using the System.out object, inputs via the console is accessed using System.in
        // User input is retrieved from the console using the Scanner object.
        // Here are the steps to get user input:
            // 1. Create a new scanner object
            // 2. Use the appropriate scanner for the values


        //class codes
        Scanner input = new Scanner(System.in); //The Scanner Object looks for which input to scan. In this case, it is the console (System.in)

        System.out.println("Input a number");
        int num = input.nextInt(); //The nextInt() method tells Java that it is expecting an integer value as input.

        // The scanner object has multiple methods that can be used to get the expected value:
            // nextInt() - expects an integer
            // nextDouble() - expects a double
            // nextLine() - gets the entire line as a String

        // EXCEPTION HANDLING
        // As the Scanner methods have specific data types associated with them, there will be issues when the input is not of the correct data type.
        // Exception handling refers to managing and catching run-time errors in order to safely run your code.
        // In order to understand this, we must first understand what run-time errors are and how they are different from compile-time errors.
        // Compile time errors are errors that usually happen when you try to compile a program that is syntactically incorrect or has missing package imports.
        // Run-time errors, on the other hand, are errors that happen after compilation and during the execution of the program.
        // We handle such situations in Java, which are not exclusive to switch statements and can happen in varying situations in Java, by using try-catch statements.

       // Scanner input = new Scanner(System.in);

        int num1 = 0;

        System.out.println("Please enter a number from 1 to 10");

        try { //try to do this statement
            num1 = input.nextInt();
        } catch (Exception e) { //catch any errors
            System.out.println("Invalid input");
            e.printStackTrace();
        }

        System.out.println("You have entered: " + num1);

        // The try-catch finally statements allows a code block to be executed regardless if an exception occurs or not. It is an optional block but it is good practice to use it frequently.
        // Note that the finally block is particularly useful when dealing with highly sensitive operations, such as:
        // Ensuring safe operations on databases by enclosing them if they are no longer needed.
        // Operations that have unpredictable situations such as it its possible for users to input problematic information
        // Any operation that may be susceptible to abuse and malicious acts such as downloading or saving files.









    }

}