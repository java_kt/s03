import java.util.Scanner;


// 1. Create a new Java project named "WDC043_S3_A1" under the base package "com.zuitt".
//2. In the main method of the Main class,  output a static message in the console asking for an integer whose factorial will be computed.
//3. Instantiate an object from Java's Scanner class, passing in System.in as an argument to the Scanner() constructor. Save this object as a variable named "in".
//4. Using the nextInt() method of the Scanner object, receive the next console input of data type int and save it as a variable named "num".
//5. Declare and initialize an answer variable and a counter variable, both of which are initially set to 1.
//6. Use a while loop to calculate for the factorial of the received integer.
//7. Have the students create a control structure to take into account the number zero and negative values.
//8. Implement the same solution using a for loop.
//9. To implement exception handling, use try-catch on the step where user input is needed prior to the factorial computation.

public class Main {
    public static void main(String[] args) {


        System.out.println("Hello world!");
        Scanner input = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed:");
        int num = input.nextInt();
        System.out.println(num);

        int factorial =  num * (num - 1);
        System.out.println(factorial);

        if (num == 1) {
            int i = 1;
            return int i;
        } else {
            return factorial;
        }


    }
}